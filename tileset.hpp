#ifndef _RR_TILESET_HPP
#define _RR_TILESET_HPP

#include <vector>

#include "tile.hpp"

class Tileset
{
	public:
		
		Tileset(const char *filename);
		~Tileset();
		
		int NumTiles(void);			// Returns the number of tiles in 
									// the tileset
									
		Tile getTile(int num);
		
	private:
	
		std::vector <Tile> _tiles;
	
};

#endif /* _RR_TILESET_HPP */
