#ifndef _RR_ITEMMAP_HPP
#define _RR_ITEMMAP_HPP

#include <vector>

#include "map.hpp"
#include "item.hpp"

class ItemMap
{
	public:

		ItemMap(Map map);
		~ItemMap();
		
		bool hasItem(int row, int col);

		Item getItem(int row, int col);

		void setItem(Item item, int row, int col);

		int Width(void);
		int Height(void);

		/// Returns Item with -1 Id if it fails.
		Item tryPickup(int row, int col);

	private:

		int _width;
		int _height;
		std::vector<Item> _items;

};

void addTestItems(ItemMap &map);

#endif /* _RR_ITEMMAP_HPP */
