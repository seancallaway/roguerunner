/*
 * colors.hpp
 * 
 * Defines color constants.
 * 
 * Written by Sean Callaway.
 * 
 */

#ifndef _RR_COLORS_HPP
#define _RR_COLORS_HPP

#define BLACK 	1
#define RED		2
#define GREEN	3
#define YELLOW	4
#define BLUE	5
#define PINK	6
#define CYAN	7
#define WHITE	8

#endif /* _RR_COLORS_HPP */
