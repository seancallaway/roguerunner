CC=g++
LIBS=-lncurses
OBJ=screen.o player.o tile.o map.o tileset.o item.o itemmap.o main.o
FLAGS=-std=c++11

%.o: %.cc
	$(CC) $(FLAGS) -c $< $(LIBS) -o $@
	
roguerunner: $(OBJ)
	$(CC) $(FLAGS) $^ $(LIBS) -o roguerunner
	
clean:
	rm *.o
