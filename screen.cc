#include <ncurses.h>

#include "map.hpp"
#include "itemmap.hpp"
#include "item.hpp"
#include "screen.hpp"
#include "player.hpp"
#include "colors.hpp"

Screen::Screen(void)
{
	initscr();
	clear();
	noecho();
	cbreak();
	keypad(stdscr, TRUE);
	curs_set(0);
	start_color();
	
	init_pair(1, COLOR_BLACK, COLOR_BLACK);
	init_pair(2, COLOR_RED, COLOR_BLACK);
	init_pair(3, COLOR_GREEN, COLOR_BLACK);
	init_pair(4, COLOR_YELLOW, COLOR_BLACK);
	init_pair(5, COLOR_BLUE, COLOR_BLACK);
	init_pair(6, COLOR_MAGENTA, COLOR_BLACK);
	init_pair(7, COLOR_CYAN, COLOR_BLACK);
	init_pair(8, COLOR_WHITE, COLOR_BLACK);
	
	// Get screen dimensions and load into private members.
	getmaxyx(stdscr, _height, _width);
}

Screen::~Screen()
{
	endwin();
}

void Screen::Add(const char *m)
{
	printw(m);
}

int Screen::Height(void)
{
	return _height;
}

int Screen::Width(void)
{
	return _width;
}

void Screen::Draw(Map &map)
{
	for (int y = 0; y < map.Height(); y++)
	{
		for (int x = 0; x < map.Width(); x++)
		{
			Tile current = map.getTile(y, x);
			mvaddch(y, x, current.Symbol() | COLOR_PAIR(current.Color()));
		}
	}
}

void Screen::Draw(ItemMap &map)
{
	for (int y = 0; y < map.Height(); y++)
	{
		for (int x = 0; x < map.Width(); x++)
		{
			if(map.hasItem(y, x))
			{
				Item item = map.getItem(y, x);
				mvaddch(y, x, item.Symbol() | COLOR_PAIR(item.Color()));
			}
		}
	}
}

void Screen::ShowInventory(Player &player)
{
	
	// Blank area
	for (int y = 1; y < 13; y++)
	{
		for (int x = (_width / 2) - 8; x < (_width / 2) + 8; x++)
		{
			mvaddch(y, x, ' ' | COLOR_PAIR(1));
		}
	}
	
	// Draw Title
	attron(A_BOLD | COLOR_PAIR(2));
	mvprintw(1, (_width / 2) - 8, "INVENTORY"); 
	attroff(A_BOLD | COLOR_PAIR(2));
	mvprintw(2, (_width / 2) - 8, "---------"); 
	
	
	// Add all items
	for (int i = 0; i < 10; i++)
	{
		char letter = i + 65;
		mvprintw(3 + i, (_width / 2) - 8, "%c  %s", letter, player.getItem(i).Name());
	}
	refresh();
	getch();
}
