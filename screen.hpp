#ifndef _RR_SCREEN_HPP
#define _RR_SCREEN_HPP

#include "map.hpp"
#include "itemmap.hpp"
#include "player.hpp"

class Screen
{
	public:
		///
		/// Initializes the ncurses library.
		///
		Screen(void);
		
		///
		/// Clears ncurses.
		///
		~Screen();
		
		///
		/// Prints a message on the screen.
		///
		void Add(const char *message);
		
		void Draw(Map &map);

		void Draw(ItemMap &map);
		
		void ShowInventory(Player &player);
		
		///
		/// Gets the screen width and height.
		///
		int Height(void);
		int Width(void);
		
	
	private:
		int _height;
		int _width;
};

#endif /* _RR_SCREEN_HPP */
