#include <ncurses.h>
#include "tile.hpp"
#include "colors.hpp"

Tile::Tile(int i, char s, bool p, short c)
{
	_id = i;
	_symbol = s;
	_passable = p;
	_color = c;
}

Tile::~Tile()
{
	// Nothing to do.
}

int Tile::Id(void)
{
	return _id;
}

char Tile::Symbol(void)
{
	return (_symbol);
}

bool Tile::Passable(void)
{
	return _passable;
}

short Tile::Color(void)
{
	return _color;
}
