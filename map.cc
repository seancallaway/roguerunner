#include <vector>
#include <fstream>
#include <string>

#include <ncurses.h>

#include "map.hpp"
#include "tile.hpp"
#include "colors.hpp"
#include "tileset.hpp"

Map::Map(int width, int height, const char *tf) : _tileset(tf)
{
	_width = width;
	_height = height;
}

Map::~Map()
{
	
}

Tile Map::getTile(int y, int x)
{
	if ((y < _height) && (x < _width))
	{
		int temp = _tiles[(y * _width) + x];
		return _tileset.getTile(temp);
	}
	
	Tile nullTile(-1, '\0', false, BLACK);
	return nullTile;
}

void Map::setTile(int t, int y, int x)
{
	if ((y < _height) && (x < _width))
	{
		_tiles[(y * _width) + x] = t;
	}
}

int Map::Height(void)
{
	return _height;
}

int Map::Width(void)
{
	return _width;
}

void Map::generate(void)
{
	int length = _width * _height;
	int grass = 0;
	int wall = 2;
	
	_tiles.resize(length, grass);
	
	setTile(wall, 2, 2);
	setTile(wall, 2, 3);
	setTile(wall, 2, 4);
	setTile(wall, 2, 5);
	setTile(wall, 2, 6);
	
	setTile(wall, 3, 2);
	setTile(wall, 3, 6);
	
	setTile(wall, 4, 2);
	setTile(wall, 4, 6);
	
	setTile(wall, 5, 2);
	setTile(wall, 5, 6);
	
	setTile(wall, 6, 2);
	setTile(wall, 6, 6);
	
	setTile(wall, 7, 2);
	setTile(wall, 7, 3);
	setTile(wall, 7, 5);
	setTile(wall, 7, 6);
	
}

bool Map::loadMapFromFile(const char *filename)
{
	std::ifstream mapFile;
	
	mapFile.open(filename);
	
	if (mapFile.is_open() != true)
	{
		return false;
	}
	
	std::string header;
	
	// Ensure RRM header is there
	std::getline(mapFile, header);
	if (header.compare("RRM-1.0") != 0)
	{
		mapFile.close();
		return false;
	}
	
	int width = 0, height = 0;
	std::vector<int> temp;
	
	mapFile >> width;
	mapFile >> height;
	
	_height = height;
	_width = width;
	
	for (int i = 0; i < (width * height); i++)
	{
		int tileID = 0;
		mapFile >> tileID;
		temp.push_back(tileID);
	}
	
	_tiles = temp;
	
	mapFile.close();
	
	return true;
}

void Map::toggleDoor(int row, int col)
{
	// Check DOWN
	if (getTile(row + 1, col).Id() == 3)
	{
		setTile(4, row + 1, col);
	}
	else if (getTile(row + 1, col).Id() == 4)
	{
		setTile(3, row + 1, col);
	}
	
	// CHECK UP
	if (getTile(row - 1, col).Id() == 3)
	{
		setTile(4, row - 1, col);
	}
	else if (getTile(row - 1, col).Id() == 4)
	{
		setTile(3, row - 1, col);
	}
	
	// CHECK RIGHT
	if (getTile(row, col + 1).Id() == 3)
	{
		setTile(4, row, col + 1);
	}
	else if (getTile(row, col + 1).Id() == 4)
	{
		setTile(3, row, col + 1);
	}
	
	// CHECK LEFT
	if (getTile(row, col - 1).Id() == 3)
	{
		setTile(4, row, col - 1);
	}
	else if (getTile(row, col - 1).Id() == 4)
	{
		setTile(3, row, col - 1);
	}
}

///
/// Determines if the tile can be moved into.
///
bool Map::canMove(int row, int col)
{
	if ((row < 0) || (row >= _height)
		|| (col < 0) || (col >= _width))
	{
		// Out-of-bounds
		return false;
	} 
	
	Tile testTile = getTile(row, col);
	return testTile.Passable();
}
