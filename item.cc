#include "item.hpp"

Item::Item(void)
{
	_id = -1;
	_name = "Empty";
	_symbol = ' ';
	_color = 0;
}

Item::Item(int id, char *n, char s, short c)
{
	_id = id;
	_name = n;
	_symbol = s;
	_color = c;
}

Item::~Item()
{
	// Nothing to do here.
}

int Item::Id(void)
{
	return _id;
}

const char* Item::Name(void)
{
	return _name;
}

char Item::Symbol(void)
{
	return _symbol;
}

short Item::Color(void)
{
	return _color;
}
