#ifndef _RR_PLAYER_HPP
#define _RR_PLAYER_HPP

#include "item.hpp"

class Player
{
	public:
	
		Player(int row, int col, char symbol);
		~Player();
		
		///
		/// Returns the row and column of the Player
		///
		int Row(void);
		int Col(void);
		
		///
		/// Sets the row and column of the Player
		///
		void Row(int row);
		void Col(int col);
		
		///
		/// Returns the symbol representing the Player
		///
		char Symbol(void);
		
		///
		///	Returns the Item at the given position.
		///
		Item getItem(short pos);
		
		///
		/// Removes the Item at the given position from the inventory.
		///
		void delItem(short pos);
		
		///
		///	Adds an item to the inventory. Returns false if full.
		///
		bool addItem(Item item);

		///
		///	Adds the given amount to the player's bank.
		///	Use negative amounts to remove money. DUH.
		///
		void addGold(int amount);

		///
		///	Returns the amount of money in the player's bank.
		///
		int getGold(void); 
	
	private:
	
		int _row;
		int _col;
		char _symbol;
		int _bank;
		Item _inventory[10];
};

#endif /* _RR_PLAYER_HPP */
