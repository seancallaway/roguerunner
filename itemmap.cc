#include <vector>

#include "itemmap.hpp"
#include "item.hpp"
#include "map.hpp"


ItemMap::ItemMap(Map map) : _items()
{
	_height = map.Height();
	_width = map.Width();
	
	Item nonItem(-1, "Nothing", ' ', 0);
	
	for (int i = 0; i < _width * _height; i++)
	{
		_items.push_back(nonItem);
	}
}

ItemMap::~ItemMap()
{

}

bool ItemMap::hasItem(int row, int col)
{
	if (row < _height && col < _width)
	{
		if (_items[(row * _width) + col].Id() > -1)
		{
			return true;
		}
	}
	
	return false;
}

Item ItemMap::getItem(int row, int col)
{
	if (hasItem(row, col))
	{
		return _items[(row * _width) + col];
	}
	
	Item nonItem(-1, "Nothing", ' ', 0);
	return nonItem;
}

void ItemMap::setItem(Item item, int row, int col)
{
	if (row < _height && col < _width)
	{
		_items[(row * _width) + col] = item;
	}
}

int ItemMap::Height(void)
{
	return _height;
}

int ItemMap::Width(void)
{
	return _width;
}

Item ItemMap::tryPickup(int row, int col)
{
	Item nonItem(-1, "Nothing", ' ', 0);
	if (hasItem(row, col))
	{
		Item item = getItem(row, col);
		setItem(nonItem, row, col);
		return item;
	}

	return nonItem;	
}

void addTestItems(ItemMap &map)
{
	Item bow(1, "Bow", ')', 4);
	Item vest(2, "Jerkin", 'H', 3);

	map.setItem(bow, 3, 5);
	map.setItem(vest, 18, 2);
}
