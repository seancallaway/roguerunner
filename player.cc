#include "player.hpp"

Player::Player(int r, int c, char s)
{
	_row = r;
	_col = c;
	_symbol = s;
}

Player::~Player()
{
	// Nothing to do here.
}

int Player::Row(void)
{
	return _row;
}

void Player::Row(int r)
{
	_row = r;
}

int Player::Col(void)
{
	return _col;
}

void Player::Col(int c)
{
	_col = c;
}

char Player::Symbol(void)
{
	return _symbol;
}

Item Player::getItem(short pos)
{
	if (pos < 10 && pos > -1)
	{
		return _inventory[pos];
	}
	
	Item nullItem(-1, "EMPTY", ' ', 0);
	return nullItem;
}
		
void Player::delItem(short pos)
{
	if (pos < 10 && pos > -1)
	{
		Item nullItem(-1, "EMPTY", ' ', 0);
		_inventory[pos] = nullItem;
	}
}

bool Player::addItem(Item item)
{
	bool added = false;
	
	for (int i = 0; i < 10; i++)
	{
		if (_inventory[i].Id() == -1)
		{
			_inventory[i] = item;
			added = true;
			break;
		}
	}
	
	return added;
}

void Player::addGold(int amt)
{
	_bank += amt;
}

int Player::getGold(void)
{
	return _bank;
}
