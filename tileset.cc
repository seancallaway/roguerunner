#include <vector>
#include <fstream>
#include <string>

#include "tileset.hpp"
#include "tile.hpp"
#include "colors.hpp"

Tileset::Tileset(const char *filename)
{
	std::ifstream rrt;
	
	rrt.open(filename);
	
	if (rrt.is_open())
	{
		std::string data;
		bool validFile = false;
		int counter = 0;
		
		// Read 1st line to verify format
		std::getline(rrt, data);
		if (data.compare("RRT-1.0") == 0)
		{
			validFile = true;
		}
		
		while (rrt.good() && validFile)
		{
			int tab1Pos = 0, tab2Pos = 0;
			
			// Read in tile line
			std::getline(rrt, data);
			
			// Get tab positions
			tab1Pos = data.find('\t');
			tab2Pos = data.find('\t', tab1Pos + 1);
			
			if (tab1Pos > -1)
			{
				// Parse line
				char symbol = (data.substr(0, 1)).at(0);
				int passable = std::stoi(data.substr(tab1Pos + 1, 1));
				int color = std::stoi(data.substr(tab2Pos + 1, 1));
				
				Tile temp(counter, symbol, (passable != 0), color);
				counter++;
				
				_tiles.push_back(temp);
			}
			else
			{
				// Blank or improperly formatted line.
				validFile = false;
			}
		}
		
		rrt.close();
	}	
}

Tileset::~Tileset()
{

}

int Tileset::NumTiles(void)
{
	return _tiles.size();
}

Tile Tileset::getTile(int num)
{
	if (num < 0)
	{
		Tile nullTile(-1, ' ', false, BLACK);
		return nullTile;
	}
	return _tiles[num];
}
