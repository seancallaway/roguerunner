#include <ncurses.h>

#include "screen.hpp"
#include "player.hpp"
#include "tile.hpp"
#include "map.hpp"
#include "colors.hpp"
#include "itemmap.hpp"

int main(void)
{
	Screen screen;
	Player player(0, 0, '@');
	Map map(40, 20, "default.rrt");
	map.loadMapFromFile("test.rrm");
	ItemMap itemmap(map);	
	
	addTestItems(itemmap);
	
	bool isPlaying = true;
	int	keyIn = 0;
	
	///
	/// MAIN GAME LOOP
	///
	do
	{
		/// OUTPUT PHASE
		clear();
		screen.Draw(map);
		screen.Draw(itemmap);
		mvaddch(player.Row(), player.Col(), player.Symbol());
		refresh();
		
		
		/// INPUT PHASE
		keyIn = getch();
			
		
		/// PROCESSING PHASE
		switch (keyIn)
		{
			case KEY_LEFT:
				if (map.canMove(player.Row(), player.Col() - 1))
				{
					player.Col(player.Col() - 1);
				}
				break;
			case KEY_RIGHT:
				if (map.canMove(player.Row(), player.Col() + 1))
				{
					player.Col(player.Col() + 1);
				}
				break;
			case KEY_DOWN:
				if (map.canMove(player.Row() + 1, player.Col()))
				{
					player.Row(player.Row() + 1);
				}
				break;
			case KEY_UP:
				if (map.canMove(player.Row() - 1, player.Col()))
				{
					player.Row(player.Row() - 1);
				}
				break;
			case 'd':
				// Manipulate door
				map.toggleDoor(player.Row(), player.Col());
				break;
			case 'g':
				// Get item
				{
				// limits tempItem to this case statement and prevents "crosses initialization" errors
					Item tempItem = itemmap.tryPickup(player.Row(), player.Col());
					if (tempItem.Id() > -1)
					{
						if(player.addItem(tempItem) == false)
						{
							// can't take it, put it back
							itemmap.setItem(tempItem, player.Row(), player.Col());
						}
					}
				}
				break;
			case 'i':
				// Show inventory
				screen.ShowInventory(player);
				break;
			case 'q':
				isPlaying = false;
				break;
		}
		
	} while (isPlaying);
	
	
	return 0;
}
