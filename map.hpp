#ifndef _RR_MAP_HPP
#define _RR_MAP_HPP

#include <vector>

#include "tile.hpp"
#include "tileset.hpp"

class Map
{
	public:
		Map(int width, int height, const char *tilesetFilename);
		~Map();
		
		Tile getTile(int y, int x);
		
		int Width(void);
		int Height(void);
		
		void setTile(int tileId, int y, int x);
		
		void toggleDoor(int row, int col);
		
		bool canMove(int row, int col);
		
		void generate(void);		// Generates data for the map.
									// Should be using Perlin noise,
									// but is currently static.
		
		bool loadMapFromFile(const char *filename);
									// Loads the map from an RRM-format
									// file and 
		
	
	private:
		int _width;
		int _height;
		Tileset _tileset;
		std::vector<int> _tiles;	// Tiles are stored here so that 
									// index = ((y * width) + x)
};

#endif
