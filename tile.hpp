#ifndef _RR_TILE_HPP
#define _RR_TILE_HPP

class Tile
{
	public:
		Tile(int id, char symbol, bool passable, short color);
		~Tile();
		
		bool Passable(void);
		char Symbol(void);
		int Id(void);	
		short Color(void);
	
	private:
		bool _passable;
		char _symbol;
		int _id;
		short _color;
};

#endif /* _RR_TILE_HPP */
