#ifndef _RR_ITEM_HPP
#define _RR_ITEM_HPP

class Item
{
	public:

		Item(void);
		Item(int id, char *name, char symbol, short color);
		~Item();
		
		int Id(void);
		char Symbol(void);
		const char* Name(void);
		short Color(void);		
	
	private:
	
		int _id;
		char *_name;
		char _symbol;
		short _color;
	
};

#endif /* _RR_ITEM_HPP */
